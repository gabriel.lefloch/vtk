/*=========================================================================

  Author: Gabriel Lefloch

=========================================================================*/
/**
 * @class   vtkGlyphFunction3D
 * @brief   copy user-defined function at every point of input geometry.
 * [TODO]
 *
 * @warning
 * [TODO]
 *
 * @sa
 * vtkTensorGlyph, vtkGlyphFunction3D
 */

#ifndef vtkGlyphFunction3D_h
#define vtkGlyphFunction3D_h

#include "vtkFiltersCoreModule.h" // For export macro
#include "vtkPolyDataAlgorithm.h"
#include "vtkStdString.h"

#define USE_VECTOR_MAGNITUDE 0
#define USE_VECTOR_COMPONENTS 1

#define SCALE_BY_SCALAR 0
#define SCALE_BY_VECTOR_MAGNITUDE 1
#define SCALE_BY_VECTOR_COMPONENTS 2
#define SCALE_BY_NORMAL_MAGNITUDE 3
#define SCALE_BY_NORMAL_COMPONENTS 4
#define SCALE_BY_FIELDARRAY 5
#define SCALE_OFF 6

#define COLOR_BY_SCALAR 0
#define COLOR_BY_VECTOR_MAGNITUDE 1
#define COLOR_BY_NORMAL_MAGNITUDE 2
#define COLOR_BY_FIELDARRAY 3
#define COLOR_OFF 4

#define ROTATE_BY_VECTOR 0
#define ROTATE_BY_NORMAL 1
#define ROTATE_BY_TENSOR 2
#define ROTATE_BY_CAMERA_DIRECTION 3
#define ROTATE_BY_FIELDARRAY 4
#define ROTATE_OFF 5

#define DISPLACEMENT_BY_RANDOM 0
#define DISPLACEMENT_BY_VECTOR 1
#define DISPLACEMENT_BY_NORMAL 2
#define DISPLACEMENT_BY_FIELDARRAY 3
#define DISPLACEMENT_OFF 4

class vtkTransform;

class VTKFILTERSCORE_EXPORT vtkGlyphFunction3D : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkGlyphFunction3D, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  typedef void (*ProgrammableMethodCallbackType)(void* arg);


  /**
   * Construct object
   * Todo: detail defaults
   */
  static vtkGlyphFunction3D* New();

  /**
   * Setup a connection for the source to use as the glyph.
   * Note: you can change the source during execution of this filter.
   * This is equivalent to SetInputConnection(1, output);
   */
  void SetSourceConnection(vtkAlgorithmOutput* output);

  ///@{
  /**
   * Set/Get the source to use for this glyph.
   * Note that SetSourceData() does not set a pipeline connection but
   * directly uses the polydata.
   */
  void SetSourceData(vtkPolyData* source);
  vtkPolyData* GetSource();
  ///@}

  /**
   * Set the source function used to generate the source polyData.
   */
  void SetSourceMethod(void (*f)(void*), void* arg);

  /**
   * Set the arg delete method. This is used to free user memory that might
   * be associated with the GlyphMethod().
   */
  void SetSourceMethodArgDelete(void (*f)(void*));

  ///@{
  /**
   * Get the current point id during processing. Value only valid during the
   * Execute() method of this filter. (Meant to be called by the GlyphMethod().)
   */
  vtkGetMacro(PointId, vtkIdType);
  ///@}

  ///@{
  /**
   * Get the current point coordinates during processing. Value only valid during the
   * Execute() method of this filter. (Meant to be called by the GlyphMethod().)
   */
  vtkGetVector3Macro(Point, double);
  ///@}

  ///@{
  /**
   * Get the set of point data attributes for the input. A convenience to the
   * programmer to be used in the GlyphMethod(). Only valid during the Execute()
   * method of this filter.
   */
  vtkGetObjectMacro(PointData, vtkPointData);
  ///@}

  /**
   * Specify the function to use to fill in information about the source data.
   */
  void SetRequestInformationMethod(ProgrammableMethodCallbackType f);\

  vtkSetMacro(ScaleMode, int);
  vtkGetMacro(ScaleMode, int);
  vtkSetMacro(ScaleFieldArrayName, vtkStdString);
  vtkGetMacro(ScaleFieldArrayName, vtkStdString);
  void SetScaleModeToScalar(){ this->SetScaleMode(SCALE_BY_SCALAR); };
  void SetScaleModeToVectorMagnitude(){ this->SetScaleMode(SCALE_BY_VECTOR_MAGNITUDE); };
  void SetScaleModeToVectorComponents(){ this->SetScaleMode(SCALE_BY_VECTOR_COMPONENTS); };
  void SetScaleModeToNormalMagnitude(){ this->SetScaleMode(SCALE_BY_NORMAL_MAGNITUDE); };
  void SetScaleModeToNormalComponents(){ this->SetScaleMode(SCALE_BY_NORMAL_COMPONENTS); };
  void SetScaleModeToFieldArray(const vtkStdString arrayName){
    this->SetScaleMode(SCALE_BY_FIELDARRAY);
    this->SetScaleFieldArrayName(arrayName);
  };
  void SetScaleModeOff(){ this->SetScaleMode(SCALE_OFF); };

  vtkSetMacro(ColorMode, int);
  vtkGetMacro(ColorMode, int);
  vtkSetMacro(ColorFieldArrayName, vtkStdString);
  vtkGetMacro(ColorFieldArrayName, vtkStdString);
  void SetColorModeToScalar(){ this->SetColorMode(COLOR_BY_SCALAR); };
  void SetColorModeToVectorMagnitude(){ this->SetColorMode(COLOR_BY_VECTOR_MAGNITUDE); };
  void SetColorModeToNormalMagnitude(){ this->SetColorMode(COLOR_BY_NORMAL_MAGNITUDE); };
  void SetColorModeToFieldArray(const vtkStdString arrayName){
    this->SetColorMode(COLOR_BY_FIELDARRAY);
    this->SetColorFieldArrayName(arrayName);
  };
  void SetColorModeOff(){ this->SetColorMode(COLOR_OFF); };

  vtkSetMacro(RotationMode, int);
  vtkGetMacro(RotationMode, int);
  vtkSetMacro(RotationFieldArrayName, vtkStdString);
  vtkGetMacro(RotationFieldArrayName, vtkStdString);
  void SetRotationModeToVector(){ this->SetRotationMode(ROTATE_BY_VECTOR); };
  void SetRotationModeToNormal(){ this->SetRotationMode(ROTATE_BY_NORMAL); };
  void SetRotationModeToTensor(){ this->SetRotationMode(ROTATE_BY_TENSOR); };
  void SetRotationModeToFieldArray(const vtkStdString arrayName){
    this->SetRotationMode(ROTATE_BY_FIELDARRAY);
    this->SetRotationFieldArrayName(arrayName);
  };
  void SetRotationModeToCamera(){ this->SetRotationMode(ROTATE_BY_CAMERA_DIRECTION); };
  void SetRotationModeOff(){ this->SetRotationMode(ROTATE_OFF); };

  vtkSetMacro(DisplacementMode, int);
  vtkGetMacro(DisplacementMode, int);
  vtkSetMacro(DisplacementFieldArrayName, vtkStdString);
  vtkGetMacro(DisplacementFieldArrayName, vtkStdString);
  void SetDisplacementModeToRandom(){ this->SetDisplacementMode(DISPLACEMENT_BY_RANDOM); };
  void SetDisplacementModeToVector(){ this->SetDisplacementMode(DISPLACEMENT_BY_VECTOR); };
  void SetDisplacementModeToNormal(){ this->SetDisplacementMode(DISPLACEMENT_BY_NORMAL); };
  void SetDisplacementModeToFieldArray(const vtkStdString arrayName){
    this->SetDisplacementMode(DISPLACEMENT_BY_FIELDARRAY);
    this->SetDisplacementFieldArrayName(arrayName);
  };
  void SetDisplacementModeOff(){ this->SetDisplacementMode(DISPLACEMENT_OFF); };

  vtkSetMacro(ScaleFactor, double);
  vtkGetMacro(ScaleFactor, double);

  /**
   * This can be overwritten by subclass to return 0 when a point is
   * blanked. Default implementation is to always return 1;
   */
  virtual int IsPointVisible(vtkDataSet*, vtkIdType) { return 1; };

  /**
   * Overridden to include SourceTransform's MTime.
   */
  vtkMTimeType GetMTime() override;

  ///@{
  /**
   * Set/get the desired precision for the output types. See the documentation
   * for the vtkAlgorithm::DesiredOutputPrecision enum for an explanation of
   * the available precision settings.
   */
  vtkSetMacro(OutputPointsPrecision, int);
  vtkGetMacro(OutputPointsPrecision, int);
  ///@}

protected:
  vtkGlyphFunction3D();
  ~vtkGlyphFunction3D() override;

  int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*) override;
  int RequestUpdateExtent(vtkInformation*, vtkInformationVector**, vtkInformationVector*) override;
  int FillInputPortInformation(int, vtkInformation*) override;

  ProgrammableMethodCallbackType SourceMethod; // function to invoke
  ProgrammableMethodCallbackType SourceMethodArgDelete; // function to invoke
  void* SourceMethodArg;

  vtkTimeStamp ExecuteTime;

  int ScaleMode;
  int ColorMode;
  int RotationMode;
  int DisplacementMode;
  double ScaleFactor;
  double Point[3];
  vtkIdType PointId;
  vtkPointData* PointData;

  vtkStdString ScaleFieldArrayName;
  vtkStdString ColorFieldArrayName;
  vtkStdString RotationFieldArrayName;
  vtkStdString DisplacementFieldArrayName;

  vtkTypeBool Clamping;
  int OutputPointsPrecision;

private:
 

  vtkGlyphFunction3D(const vtkGlyphFunction3D&) = delete;
  void operator=(const vtkGlyphFunction3D&) = delete;
};

#endif