/*
 * Gabriel Lefloch
 * TODO: 
  - introduce data streaming for meshing and point generation
  - Add direct RGB support
  - work on new extension of superqudric function to fix x-axis drifting
  - implement symmetry axes
  - Future work: generalize number of theta and phi domains
                 generalize checkerboard-style for any polydata
 */

#include "vtkTenderSource.h"

#include "vtkFloatArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMath.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkColorTransferFunction.h"
#include "vtkLookupTable.h"

#include <string>

VTK_ABI_NAMESPACE_BEGIN
vtkStandardNewMacro(vtkTenderSource);

namespace {
  const double eps = 1e-12;
  const double pi = vtkMath::Pi();
  const double pi_two = pi/2.0;

  inline bool isNull(double a){
    if( abs(a) < eps ){
      return true;
    }
    return false;
  }
  double cf(double w, double m){
    double c;
    double sgn;

    if (w == vtkMath::Pi() || w == -vtkMath::Pi()){
      c = -1.0;
    }
    else{
      c = std::cos(w);
    }
    sgn = c < 0.0 ? -1.0 : 1.0;
    return sgn * std::pow(sgn * c, m);
  }
  double sf(double w, double m){
    double s;
    double sgn;

    if (w == vtkMath::Pi() || w == -vtkMath::Pi()){
      s = 0.0;
    }
    else{
      s = std::sin(w);
    }
    sgn = s < 0.0 ? -1.0 : 1.0;
    return sgn * std::pow(sgn * s, m);
  }
  void evalSuperquadric(
    double theta, double phi,  // parametric coords
    double rtheta, double rphi,// roundness params
    double dims[3],            // x, y, z dimensions
    double xyz[3],             // output coords
    double nrm[3])             // output normals
  {
    // axis of symmetry: z
    double cf1, cf2;

    cf1 = cf(phi, rphi);
    xyz[0] = dims[0] * cf1 * cf(theta, rtheta);
    xyz[1] = dims[1] * cf1 * sf(theta, rtheta);
    xyz[2] = dims[2] * sf(phi, rphi);

    // Current extension/fix of superquadric definition
    if( isNull(theta-pi_two) || isNull(theta+pi_two) ){
      xyz[0] = 0;
    }

    cf2 = cf(phi, rphi);
    nrm[0] = 1.0 / dims[0] * cf2 * cf(theta, 2.0 - rtheta);
    nrm[1] = 1.0 / dims[1] * cf2 * sf(theta, 2.0 - rtheta);
    nrm[2] = 1.0 / dims[2] * sf(phi, 2.0 - rphi);
  }
  void SetPointSymmetry(int axis, double pt[3]){
    double tmp;
    switch (axis) {
      case 0:
        // x-axis
        tmp = pt[0];
        pt[0] = pt[2];
        pt[2] = tmp;
        pt[1] = -pt[1];
        break;
      case 1:
        // y-axis
        tmp = pt[1];
        pt[1] = pt[2];
        pt[2] = tmp;
        pt[0] = -pt[0];
        break;
      case 2:
      default:
        // Default case is managed above
        break;
    }
  }
}

vtkTenderSource::vtkTenderSource():PhiDomains(2){
  this->SetNumberOfInputPorts(0);
  this->SetNumberOfOutputPorts(2);

  this->OutputPointsPrecision = vtkAlgorithm::SINGLE_PRECISION;

  this->AxisOfSymmetry = 2; // z-axis symmetry
  this->Center[0] = this->Center[1] = this->Center[2] = 0.0;
  for (int i = 0; i < 6; i++) {
    this->Scale[i] = 0.0;
  }
  this->Size = 1;

  this->ThetaRoundness[0] = 1.0;
  this->ThetaRoundness[1] = 1.0;
  this->PhiRoundness[0] = 1.0;
  this->PhiRoundness[1] = 1.0;
  this->SetThetaRoundness(1.0, 1.0);
  this->SetPhiRoundness(1.0, 1.0);

  this->SetThetaResolution(8);
  this->SetPhiResolution(8);

  this->Color[0] = 1.0;
  this->Color[1] = 1.0;

  this->ThetaDomains = 4;
  this->SetTetradicDomain();
}

void vtkTenderSource::SetPhiResolution(int i){
  if (i < 4){
    i = 4;
  }

  if (i > VTK_MAX_TENDER_RESOLUTION){
    i = VTK_MAX_TENDER_RESOLUTION;
  }

  if (this->PhiResolution != i){
    this->PhiResolution = i;
    this->Modified();
  }
}

void vtkTenderSource::SetThetaResolution(int i){
  if (i < 4){
    i = 4;
  }
  if (i > VTK_MAX_TENDER_RESOLUTION){
    i = VTK_MAX_TENDER_RESOLUTION;
  }

  if (this->ThetaResolution != i){
    this->ThetaResolution = i;
    this->Modified();
  }
}

void vtkTenderSource::SetThetaRoundness(double r1, double r2){
  if (r1 < VTK_MIN_TENDER_ROUNDNESS){
    r1 = VTK_MIN_TENDER_ROUNDNESS;
  }
  if (r1 > VTK_MAX_TENDER_ROUNDNESS){
    r1 = VTK_MAX_TENDER_ROUNDNESS;
  }
  if (r2 < VTK_MIN_TENDER_ROUNDNESS){
    r2 = VTK_MIN_TENDER_ROUNDNESS;
  }
  if (r2 > VTK_MAX_TENDER_ROUNDNESS){
    r2 = VTK_MAX_TENDER_ROUNDNESS;
  }

  if (this->ThetaRoundness[0] != r1){
    this->ThetaRoundness[0] = r1;
    this->Modified();
  }
  if (this->ThetaRoundness[1] != r2){
    this->ThetaRoundness[1] = r2;
    this->Modified();
  }
}

void vtkTenderSource::SetPhiRoundness(double r1, double r2){
  if (r1 < VTK_MIN_TENDER_ROUNDNESS){
    r1 = VTK_MIN_TENDER_ROUNDNESS;
  }
  else if (r1 > VTK_MAX_TENDER_ROUNDNESS){
    r1 = VTK_MAX_TENDER_ROUNDNESS;
  }
  if (r2 < VTK_MIN_TENDER_ROUNDNESS){
    r2 = VTK_MIN_TENDER_ROUNDNESS;
  }
  else if (r2 > VTK_MAX_TENDER_ROUNDNESS){
    r2 = VTK_MAX_TENDER_ROUNDNESS;
  }

  if (this->PhiRoundness[0] != r1){
    this->PhiRoundness[0] = r1;
    this->Modified();
  }
  if (this->PhiRoundness[1] != r2){
    this->PhiRoundness[1] = r2;
    this->Modified();
  }
}

void vtkTenderSource::SetDyadicDomain(){
  this->ThetaDomains = 2;
  this->Modified();
}
void vtkTenderSource::SetTetradicDomain(){
  this->ThetaDomains = 4;
  this->Modified();
}

int vtkTenderSource::RequestData(vtkInformation* vtkNotUsed(request),
  vtkInformationVector** vtkNotUsed(inputVector), vtkInformationVector* outputVector){

  // get both info object
  vtkInformation* outInfo0 = outputVector->GetInformationObject(0);
  vtkInformation* outInfo1 = outputVector->GetInformationObject(1);

  // get both outputs
  vtkPolyData* output0 = vtkPolyData::SafeDownCast(outInfo0->Get(vtkDataObject::DATA_OBJECT()));
  vtkPolyData* output1 = vtkPolyData::SafeDownCast(outInfo1->Get(vtkDataObject::DATA_OBJECT()));

  // Output variables
  vtkSmartPointer<vtkPoints> newPoints0 = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkFloatArray> newNormals0 = vtkSmartPointer<vtkFloatArray>::New();
  vtkSmartPointer<vtkFloatArray> newTCoords0 = vtkSmartPointer<vtkFloatArray>::New();
  vtkSmartPointer<vtkCellArray> newPolys0 = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkFloatArray> newColors0 = vtkSmartPointer<vtkFloatArray>::New();

  vtkSmartPointer<vtkPoints> newPoints1 = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkFloatArray>newNormals1 = vtkSmartPointer<vtkFloatArray>::New();
  vtkSmartPointer<vtkFloatArray> newTCoords1 = vtkSmartPointer<vtkFloatArray>::New();
  vtkSmartPointer<vtkCellArray> newPolys1 = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkFloatArray> newColors1 = vtkSmartPointer<vtkFloatArray>::New();

  // streaming variables
  // int p, np, maxp, start, end;

  // output variables
  int i, j, d, s, i_theta, i_phi, phi_domain, theta_domain;
  int row, column, next_row, next_column;
  int center_id, bottom_id, top_id, surface_id;
  double dtheta, dphi, dthetaDomain, dphiDomain, theta, phi, norm, tmp;

  int nbPointsPerGrid = this->ThetaResolution*this->PhiResolution;
  int nbPointsPerEW = 2*(this->PhiResolution + 2);
  int nbPointsPerSN = 2*(this->ThetaResolution + 1);
  int nbPointsPerDomain = nbPointsPerGrid + nbPointsPerEW + nbPointsPerSN;
  int nbCellsPerDomain = 2*(this->PhiResolution + this->ThetaResolution - 1);

  int nbPointsPerHemisphere = this->ThetaDomains*nbPointsPerDomain;
  int nbCellsPerHemishpere = this->ThetaDomains*nbCellsPerDomain;
  int nbPtsPerStrip = 2*this->PhiResolution;

  double thetaStart = -pi, thetaEnd = pi, phiStart = -pi_two, phiEnd = pi_two;
  double thetaRange = (thetaEnd - thetaStart);
  double phiRange = (phiEnd - phiStart);

  double thetaLim[2], phiLim[2]; // Domain limits
  double dims[2][3]; // end dimensions of glyph
  double pt[3], bottom[2][3], top[2][3]; // surface, bottom, top points
  double nv[3], EW_n[2][3], SN_n[3]; // surface, east/west, south/north normals

  // Set the desired precision for the points in the output.
  if( this->OutputPointsPrecision == vtkAlgorithm::DOUBLE_PRECISION ){
    newPoints0->SetDataType(VTK_DOUBLE);
    newPoints1->SetDataType(VTK_DOUBLE);
  }
  else{
    newPoints0->SetDataType(VTK_FLOAT);
    newPoints1->SetDataType(VTK_FLOAT);
  }

  vtkIdType* ptidx = new vtkIdType[nbPtsPerStrip];

  newPoints0->Allocate( nbPointsPerHemisphere );
  newNormals0->SetNumberOfComponents( 3 );
  newNormals0->Allocate( 3*nbPointsPerHemisphere );
  newNormals0->SetName("Normals0");
  newTCoords0->SetNumberOfComponents( 2 );
  newTCoords0->Allocate( 2*nbPointsPerHemisphere );
  newTCoords0->SetName("TCoords0");

  newPoints1->Allocate( nbPointsPerHemisphere );
  newNormals1->SetNumberOfComponents( 3 );
  newNormals1->Allocate( 3*nbPointsPerHemisphere );
  newNormals1->SetName("Normals1");
  newTCoords1->SetNumberOfComponents( 2 );
  newTCoords1->Allocate( 2*nbPointsPerHemisphere );
  newTCoords1->SetName("TCoords1");


  newColors0->SetNumberOfComponents(1);
  newColors0->Allocate( nbPointsPerHemisphere );
  newColors0->SetName("ScalarColor0");

  newColors1->SetNumberOfComponents(1);
  newColors1->Allocate( nbPointsPerHemisphere );
  newColors1->SetName("ScalarColor1");

  newPolys0->AllocateEstimate(nbCellsPerHemishpere, nbPtsPerStrip);
  newPolys1->AllocateEstimate(nbCellsPerHemishpere, nbPtsPerStrip);
  
  dthetaDomain = thetaRange/(double)this->ThetaDomains;
  dphiDomain = phiRange/(double)this->PhiDomains;
  
  dtheta = dthetaDomain/(double)(this->ThetaResolution-1);
  dphi = dphiDomain/(double)(this->PhiResolution-1);

  for (i = 0; i < 6; i++ ) {
    this->Scale[i] *= this->Size;
  }

  // tmp variables to alernate between domains
  vtkSmartPointer<vtkPoints> newPoints[2] = {newPoints0, newPoints1};
  vtkSmartPointer<vtkFloatArray> newNormals[2] = {newNormals0, newNormals1};
  vtkSmartPointer<vtkFloatArray> newTCoords[2] = {newTCoords0, newTCoords1};
  vtkSmartPointer<vtkCellArray> newPolys[2] = {newPolys0, newPolys1};
  vtkSmartPointer<vtkFloatArray> newColors[2] = {newColors0, newColors1};

  dims[0][0] = this->Scale[0];
  dims[0][1] = this->Scale[1];
  dims[0][2] = this->Scale[2];
  dims[1][0] = this->Scale[3];
  dims[1][1] = this->Scale[4];
  dims[1][2] = this->Scale[5];

  for (i=0; i<3; i++) {
    bottom[0][i] = this->Center[i];
    top[0][i] = this->Center[i];
    bottom[1][i] = this->Center[i];
    top[1][i] = this->Center[i];
  }
  bottom[0][2] -= dims[0][2];
  bottom[1][2] -= dims[1][2];

  SN_n[0] = 0;
  SN_n[1] = 0;
  SN_n[2] = +1; 

  for (phi_domain = 0; phi_domain < this->PhiDomains; phi_domain++) {
    bottom[0][2] += phi_domain*dims[0][2];
    bottom[1][2] += phi_domain*dims[1][2];
    top[0][2] += phi_domain*dims[0][2];
    top[1][2] += phi_domain*dims[1][2];
    for (theta_domain = 0; theta_domain < this->ThetaDomains; theta_domain++) {
      // TODO: make more efficient version of this
      thetaLim[0] = thetaStart + theta_domain*dthetaDomain;
      thetaLim[1] = thetaLim[0] + dthetaDomain;
      phiLim[0] = phiStart + phi_domain*dphiDomain;
      phiLim[1] = phiLim[0] + dphiDomain;

      EW_n[0][0] = -std::sin(thetaLim[0]);
      EW_n[0][1] = std::cos(thetaLim[0]);
      EW_n[0][2] = 0;

      EW_n[1][0] = -std::sin(thetaLim[1]);
      EW_n[1][1] = std::cos(thetaLim[1]);
      EW_n[1][2] = 0;
      
      d = (phi_domain+theta_domain)%2; // altern between tensors
      s = newPoints[d]->GetNumberOfPoints(); // starting point index

      // Build the main surface points of current domain
      for (i_theta = 0; i_theta < this->ThetaResolution; i_theta++) {
        for (i_phi = 0; i_phi < this->PhiResolution; i_phi++) {
          theta = thetaLim[0] + i_theta*dtheta;
          phi = phiLim[0] + i_phi*dphi;

          evalSuperquadric(
            theta, phi, // parametric coords
            this->ThetaRoundness[d], this->PhiRoundness[d], dims[d], // roundness and scale
            pt, nv); // points and normals
          
          norm = vtkMath::Norm(nv);

          if( isNull(norm) ){
            norm = 1;
          }
          for (i = 0; i < 3; i++) {
            nv[i] /= norm;
            pt[i] += this->Center[i];
          }
          newPoints[d]->InsertNextPoint(pt);
          newNormals[d]->InsertNextTuple(nv);

          newColors[d]->InsertNextTuple1(this->Color[d]);
          //TODO: TCoords
        }
      }

      // Surface point meshing
      for (i_theta = 0; i_theta < this->ThetaResolution-1; i_theta++) {
        column = s + i_theta*(this->PhiResolution);
			  next_column = column + (this->PhiResolution);
        // mesh main surface
        for (i_phi = 0; i_phi < this->PhiResolution; i_phi++) {
          row = column + i_phi;
          next_row = next_column + i_phi;
          ptidx[2*i_phi] = row;
          ptidx[2*i_phi+1] = next_row;
        }
        newPolys[d]->InsertNextCell(nbPtsPerStrip, ptidx);
      }
      
    //   // East West point meshing
      for (j=0; j < 2; j++) {
        column = s + j * (nbPointsPerGrid - this->PhiResolution);

        top_id = newPoints[d]->InsertNextPoint(top[d]);
        newNormals[d]->InsertNextTuple(EW_n[j]);
        newColors[d]->InsertNextTuple1(this->Color[d]);
        
        bottom_id = newPoints[d]->InsertNextPoint(bottom[d]);
        newNormals[d]->InsertNextTuple(EW_n[j]);
        newColors[d]->InsertNextTuple1(this->Color[d]);

        if (phi_domain > 0){
          center_id = bottom_id;
        }
        else {
          center_id = top_id;
        }
        
        for (i_phi = 0; i_phi < this->PhiResolution; i_phi++) {
          row = column + i_phi;
          
          newPoints[d]->GetPoint(row, pt);
          surface_id = newPoints[d]->InsertNextPoint(pt);
          newNormals[d]->InsertNextTuple(EW_n[j]);
          newColors[d]->InsertNextTuple1(this->Color[d]);

          ptidx[0] = bottom_id + i_phi;
          ptidx[1] = top_id;
          ptidx[2] = surface_id;
          newPolys[d]->InsertNextCell(3, ptidx);
        }
      }

      // South/north point meshing
      bottom_id = newPoints[d]->InsertNextPoint(bottom[d]);
      newNormals[d]->InsertNextTuple(SN_n);
      newColors[d]->InsertNextTuple1(this->Color[d]);
      for (i_theta = 0; i_theta < this->ThetaResolution; i_theta++) {
        column = s + i_theta * this->PhiResolution;        
        newPoints[d]->GetPoint(column, pt);
        surface_id = newPoints[d]->InsertNextPoint(pt);
        newNormals[d]->InsertNextTuple(SN_n);
        newColors[d]->InsertNextTuple1(this->Color[d]);
      }
      for (i_theta = 0; i_theta < this->ThetaResolution-1; i_theta++) {
        ptidx[0] = bottom_id ;
        ptidx[1] = bottom_id + i_theta + 1;
        ptidx[2] = bottom_id + i_theta + 2;
        newPolys[d]->InsertNextCell(3, ptidx);
      }

      top_id = newPoints[d]->InsertNextPoint(top[d]);
      newNormals[d]->InsertNextTuple(SN_n);
      newColors[d]->InsertNextTuple1(this->Color[d]);
      for (i_theta = 0; i_theta < this->ThetaResolution; i_theta++) {
        column = s + (i_theta + 1) * this->PhiResolution - 1;        
        newPoints[d]->GetPoint(column, pt);
        surface_id = newPoints[d]->InsertNextPoint(pt);
        newNormals[d]->InsertNextTuple(SN_n);
        newColors[d]->InsertNextTuple1(this->Color[d]);
      }
      for (i_theta = 0; i_theta < this->ThetaResolution-1; i_theta++) {
        ptidx[0] = top_id ;
        ptidx[1] = top_id + i_theta + 1;
        ptidx[2] = top_id + i_theta + 2;
        newPolys[d]->InsertNextCell(3, ptidx);
      }
    }
  }

  delete[] ptidx;

  newPoints0->Modified();
  newPoints0->Squeeze();
  output0->SetPoints( newPoints0 );
  newNormals0->Modified();
  newNormals0->Squeeze();
  output0->GetPointData()->SetNormals( newNormals0 );
  newPolys0->Modified();
  newPolys0->Squeeze();
  output0->SetStrips( newPolys0 );
  output0->GetPointData()->SetScalars( newColors0 );

  newPoints1->Modified();
  newPoints1->Squeeze();
  output1->SetPoints( newPoints1 );
  newNormals1->Modified();
  newNormals1->Squeeze();
  output1->GetPointData()->SetNormals( newNormals1 );
  newPolys1->Modified();
  newPolys1->Squeeze();
  output1->SetStrips( newPolys1 );
  output1->GetPointData()->SetScalars( newColors1 );

  output0->Modified();
  output1->Modified();

  return 1;

}
//------------------------------------------------------------------------------
int vtkTenderSource::RequestInformation(vtkInformation* vtkNotUsed(request),
  vtkInformationVector** vtkNotUsed(inputVector), vtkInformationVector* outputVector){
  // get the info object
  vtkInformation* outInfo = outputVector->GetInformationObject(0);

  outInfo->Set(CAN_HANDLE_PIECE_REQUEST(), 0);

  return 1;
}

void vtkTenderSource::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);

  os << indent << "Axis Of Symmetry: " << this->AxisOfSymmetry << "\n";
  os << indent << "Size: " << this->Size << "\n";
  os << indent << "Theta Resolution: " << this->ThetaResolution << "\n";
  os << indent << "Phi Resolution: " << this->PhiResolution << "\n";
  os << indent << "Theta Roundness 1 and 2: (" << this->ThetaRoundness[0] <<  ", " << this->ThetaRoundness[1] << ")\n";
  os << indent << "Phi Roundness 1 and 2: (" << this->PhiRoundness[0] <<  ", " <<  this->PhiRoundness[1] << ")\n";
  os << indent << "Center: (" << this->Center[0] << ", " << this->Center[1] << ", " << this->Center[2] << ")\n";
  os << indent << "Scale: (" << this->Scale[0] << ", " << this->Scale[1] << ", " << this->Scale[2] 
  << this->Scale[3] << ", " << this->Scale[4] << ", " << this->Scale[5] << ")\n";
  os << indent << "Color 1 and 2: (" << this->Color[0] << " ," << this->Color[0] << ")\n";
  os << indent << "Output Points Precision: " << this->OutputPointsPrecision << "\n";
}
VTK_ABI_NAMESPACE_END