/*
 * Gabriel Lefloch
 * TODO: 
  - introduce data streaming for meshing and point generation
  - work on new extension of superqudric function to fix x-axis drifting
  - implement symmetry axes
  - Future work: generalize checkerboard-style for any polydata
 */

/**
 * @class   vtkTenderSource
 * @brief   Create a polygonal Tender Glyph centered at the origin
 *
 * vtkTenderSource creates a Tender glyph (represented by polygons). 
 * This object uses a checkerboard-style visualization to represent
 * two superquadric glyphs simultameously. The roundess and resolution 
 * of the two ellipsoids along the azimuthal (theta) and polar (phi) 
 * axes can be specified. The `Scale` parameter controls the scale of both
 * glyphs along all three axes, while `Size` controls the scale of the
 * entire glyph. The `Color` parameter associates a scalar to the points
 * of each sub-glyph. The checkerboard visualization can Tetradic or 
 * dyadic (i.e. hemipshere divided into 4 octants or 2 quadrants).
 * 
 * This code is based on 
 *  - "Rigid physically based superquadrics", A. H. Barr,
 * in "Graphics Gems III", David Kirk, ed., Academic Press, 1992.
 *  - Zhang et al. Glyph-Based Comparative Visualization for Diffusion 
 * Tensor Fields. IEEE Transactions on Visualization and Computer Graphics. 
 * 21. 10.1109/TVCG.2015.2467435. 
 *
 * @warning
 * Since VTK cannot represent two scalar fields at once using only a 
 * single mapper, the two domains of the Tender glyph are alloted to
 * two outputs. One can then simply create a LookupTable and mapper
 * for each output to represent both scalar fields at once.
 * 
 * @warning
 * The ThetaResolution and PhiResolution control the resolution of only
 * one domain (i.e. octant or quadrant).
 *
 * @note
 * The roundness parameters are clamped between 0 and 4.
 */


#ifndef vtkTenderSource_h
#define vtkTenderSource_h

#include "vtkFiltersSourcesModule.h" // For export macro
#include "vtkPolyDataAlgorithm.h"

#define VTK_MAX_TENDER_RESOLUTION 1024
#define VTK_MIN_TENDER_ROUNDNESS 1e-24
#define VTK_MAX_TENDER_ROUNDNESS 4

VTK_ABI_NAMESPACE_BEGIN
class VTKFILTERSSOURCES_EXPORT vtkTenderSource : public vtkPolyDataAlgorithm{
public:

  /**
   * Construct (0,0,0) centered Tender Glyph with default Theta and Phi 
   * resolutions of 8. The scale and size are of 1.0 in all directions, 
   * as well as the roundness parameters. The colors are initialzed to 
   * Colors = {-1, 1}.
   */
  static vtkTenderSource* New();

  ///@{
  /**
   * Standard methods for obtaining type information, and printing.
   */
  vtkTypeMacro(vtkTenderSource, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent) override;
  ///@}

  ///@{
  /**
   * Set the Theta and Phi roundness for both ellipsoids. 
   * Default is 1.0
   */
  vtkGetVector2Macro(ThetaRoundness, double);
  vtkGetVector2Macro(PhiRoundness, double);
  void SetThetaRoundness(double r1, double r2);
  void SetPhiRoundness(double r1, double r2);
  ///@}

  ///@{
  /**
   * Set the origin of the glyph. Default is (0,0,0).
   */
  vtkSetVector3Macro(Center, double);
  vtkGetVector3Macro(Center, double);
  ///@}

  ///@{
  /**
   * Set/Get the color of the two Superquadrics. 
   */
  vtkSetVector2Macro(Color, double);
  vtkGetVector2Macro(Color, double);
  ///@}

  ///@{
  /**
   * Set/Get the two superquadric's scales along the (x,y,z) axes,
   * Is of the form (x1, y1, z1, x2, y2, z2). Default is 1.0 everywhere.
   */
  vtkSetVector6Macro(Scale, double);
  vtkGetVector6Macro(Scale, double);
  /**
   * Set/Get the size of the Tender glyph. Default is set to 1.0.
   */
  vtkSetMacro(Size, double);
  vtkGetMacro(Size, double);
  ///@}

  ///@{
  /**
   * Set/Get the Theta and Phi resolution. Values are clamped between
   * 4 and 1024. Default is set to 8 for both.
   */
  vtkGetMacro(ThetaResolution, int);
  void SetThetaResolution(int i);
  vtkGetMacro(PhiResolution, int);
  void SetPhiResolution(int i);
  ///@}

  ///@{
  /**
   * TODO: implement 
   */
  vtkSetMacro(AxisOfSymmetry, int);
  vtkGetMacro(AxisOfSymmetry, int);
  void SetXAxisOfSymmetry() { this->SetAxisOfSymmetry(0); }
  void SetYAxisOfSymmetry() { this->SetAxisOfSymmetry(1); }
  void SetZAxisOfSymmetry() { this->SetAxisOfSymmetry(2); }
  ///@}

  ///@{
  /**
   * Set/get the desired precision for the output points.
   * vtkAlgorithm::SINGLE_PRECISION - Output single-precision floating point.
   * vtkAlgorithm::DOUBLE_PRECISION - Output double-precision floating point.
   */
  vtkSetMacro(OutputPointsPrecision, int);
  vtkGetMacro(OutputPointsPrecision, int);
  ///@}

  ///@{
  /**
   * get the number of domains used for the checker-board visualization
   * along the that and phi angles,  By default, ThetaDomains is 4 while
   * PhDomains is 2.
   */
  vtkGetMacro(ThetaDomains, int);
  vtkGetMacro(PhiDomains, int);
  /**
   * Modify the number of ThetaDomains. Can either be 2 or 4.
   * Default is 4.
   */
  void SetDyadicDomain();
  void SetTetradicDomain();
  ///@}

protected:
  vtkTenderSource();
  ~vtkTenderSource() override = default;

  int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*) override;
  int RequestInformation(vtkInformation*, vtkInformationVector**, vtkInformationVector*) override;

  double ThetaRoundness[2];
  double PhiRoundness[2];
  double Center[3];
  double Color[2];
  double Scale[6];
  double Size;
  
  int ThetaResolution;
  int PhiResolution;
  int AxisOfSymmetry;
  int OutputPointsPrecision;
  int ThetaDomains;
  const int PhiDomains;

private:
  vtkTenderSource(const vtkTenderSource&) = delete;
  void operator=(const vtkTenderSource&) = delete;
};
VTK_ABI_NAMESPACE_END
#endif
