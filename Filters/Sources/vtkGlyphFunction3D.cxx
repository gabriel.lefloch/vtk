/*=========================================================================

  Author: Gabriel Lefloch
  TODO: 
   - Manage ghosting
   - Manage blanking
   - Setup displacement methods (see litterature)
   - Corner cases for point/normal transformations (wrong orientation of normals, etc)
   - Set correct output array names
   - Handle default source if source function ain't good
   - Min glyph size for scaling
   - ...

=========================================================================*/
#include "vtkGlyphFunction3D.h"

#include "vtkCell.h"
#include "vtkCellData.h"
#include "vtkFloatArray.h"
#include "vtkIdList.h"
#include "vtkIdTypeArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMath.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkTransform.h"
#include "vtkMatrix4x4.h"
#include "vtkTrivialProducer.h"
#include "vtkUniformGrid.h"
#include "vtkUnsignedCharArray.h"
#include "vtkCommand.h"
#include "vtkExecutive.h"
#include <vtkMinimalStandardRandomSequence.h>

vtkStandardNewMacro(vtkGlyphFunction3D);

class Transformations{
private:
  const double eps = 1e-12;
  float scale = 1.0;
  double scaleFactor = 1.0, color = 0.0, p2pLength = 0.0 ;
  int ColorMode, ScaleMode, RotationMode, DisplacementMode;

  vtkGlyphFunction3D* glyphFunction3D;

  vtkTransform* transform;
  vtkMatrix4x4* matrix;
  vtkDataSet* input;
  vtkPointData* inPD;
  vtkDataArray* scalars;
  vtkDataArray* vectors;
  vtkDataArray* normals;
  vtkDataArray* tensors;
  vtkDataArray* ColorFieldArray;
  vtkDataArray* ScaleFieldArray;
  vtkDataArray* RotateFieldArray;
  vtkDataArray* DisplaceFieldArray;
  vtkMinimalStandardRandomSequence* rng;

  inline bool isNull(double a){
    if( abs(a) < eps ){
      return true;
    }
    return false;
  };

  // tmp variables
  double v[3], t[9], c1[3], c2[3], c3[3], m;

  // Function Aliases
  double (Transformations::*SetColorPtr)(int);
  void (Transformations::*SetScalePtr)(int);
  void (Transformations::*SetRotationPtr)(int);
  void (Transformations::*SetDisplacementPtr)(int);

public:

  bool hasScalars=false, 
    hasVectors=false, 
    hasNormals=false,
    hasTensors=false,
    hasColorFieldArray=false,
    hasScaleFieldArray=false,
    hasRotateFieldArray=false,
    hasDisplaceFieldArray=false;

  double SetColor(int pid){
    return (this->*SetColorPtr)(pid);
  };
  void SetScale(int pid){
    (this->*SetScalePtr)(pid);
  };
  void SetRotation(int pid){
    (this->*SetRotationPtr)(pid);
  };
  void SetDisplacement(int pid){
    (this->*SetDisplacementPtr)(pid);
  };
  void SetIdentity(){
    this->transform->Identity();
  };

  Transformations(vtkGlyphFunction3D* glyphFunction3Dpointer, vtkDataSet* inData):glyphFunction3D(glyphFunction3Dpointer), input(inData){
    this->transform = vtkTransform::New();
    this->transform->PreMultiply();
    this->matrix = vtkMatrix4x4::New();
    this->rng = vtkMinimalStandardRandomSequence::New();
    this->inPD = input->GetPointData();

    this->scalars = inPD->GetScalars();
    this->vectors = inPD->GetVectors();
    this->normals = inPD->GetNormals();
    this->tensors = inPD->GetTensors();

    this->scaleFactor = this->glyphFunction3D->GetScaleFactor();

    this->ColorMode = this->glyphFunction3D->GetColorMode();
    this->ScaleMode = this->glyphFunction3D->GetScaleMode();
    this->RotationMode = this->glyphFunction3D->GetRotationMode();
    this->DisplacementMode = this->glyphFunction3D->GetDisplacementMode();

    switch( this->ColorMode ){
      case COLOR_BY_SCALAR: // use input scalars
        this->SetColorPtr = &Transformations::ColorGlyphByScalar;
        break;
      case COLOR_BY_VECTOR_MAGNITUDE: // use input vectors
        this->SetColorPtr = &Transformations::ColorGlyphByVectorMagnitude;
        break;
      case COLOR_BY_NORMAL_MAGNITUDE: // use input vectors
        this->SetColorPtr = &Transformations::ColorGlyphByNormalMagnitude;
        break;
      case COLOR_BY_FIELDARRAY: // use user-specified array
        this->ColorFieldArray = this->input->GetPointData()->GetArray(this->glyphFunction3D->GetColorFieldArrayName().c_str());
        if (this->ColorFieldArray != nullptr) {
          this->SetColorPtr = &Transformations::ColorGlyphByFieldArray;
        }
        else{
          cout << "Field array '" << this->glyphFunction3D->GetColorFieldArrayName() << "' not found!"<< endl;
          this->SetColorPtr = &Transformations::TransformColorOff;
        }
        break;
      case COLOR_OFF: // do nothing
        this->SetColorPtr = &Transformations::TransformColorOff;
        break;
      default:
        cout << "Invalid ScaleMode!"<< endl;
    }

    switch( this->ScaleMode ){
      case SCALE_BY_SCALAR: // use input scalars
        this->SetScalePtr = &Transformations::ScaleGlyphByScalar;
        break;
      case SCALE_BY_VECTOR_MAGNITUDE: // use input vectors
        this->SetScalePtr = &Transformations::ScaleGlyphByVectorMagnitude;
        break;
      case SCALE_BY_VECTOR_COMPONENTS: // use input vectors
        this->SetScalePtr = &Transformations::ScaleGlyphByVectorComponents;
        break;
      case SCALE_BY_NORMAL_MAGNITUDE: // use input normals
        this->SetScalePtr = &Transformations::ScaleGlyphByNormalMagnitude;
        break;
      case SCALE_BY_NORMAL_COMPONENTS: // use input normals
        this->SetScalePtr = &Transformations::ScaleGlyphByNormalComponents;
        break;
      case SCALE_BY_FIELDARRAY: // use user-specified array
        this->ScaleFieldArray = this->input->GetPointData()->GetArray(this->glyphFunction3D->GetScaleFieldArrayName().c_str());
        if (this->ScaleFieldArray != nullptr) {
          this->SetScalePtr = &Transformations::ScaleGlyphByFieldArray;
        }
        else{
          cout << "Field array '" << this->glyphFunction3D->GetScaleFieldArrayName() << "' not found!" << endl;
          this->SetScalePtr = &Transformations::TransformScaleOff;
        }
        break;
      case SCALE_OFF: // do nothing
        this->SetScalePtr = &Transformations::TransformScaleOff;
        break;
      default:
        cout << "Invalid ScaleMode!" << endl;
    }

    // Setup rotation data
    switch( this->RotationMode ){
      case ROTATE_BY_VECTOR: // use input vectors
        this->SetRotationPtr = &Transformations::RotateGlyphByVector;
        break;
      case ROTATE_BY_NORMAL: // use input normals
        this->SetRotationPtr = &Transformations::RotateGlyphByNormal;
        break;
      case ROTATE_BY_TENSOR: // use input tensors
        this->SetRotationPtr = &Transformations::RotateGlyphByTensor;
        break;
      case ROTATE_BY_CAMERA_DIRECTION: // use camera direction
        this->SetRotationPtr = &Transformations::RotateGlyphByVector;
        break;
      case ROTATE_BY_FIELDARRAY: // use user-specified array
        this->RotateFieldArray = this->input->GetPointData()->GetArray(this->glyphFunction3D->GetRotationFieldArrayName().c_str());
        if (this->RotateFieldArray != nullptr) {
          this->SetRotationPtr = &Transformations::RotateGlyphByFieldArray;
        }
        else{
          cout << "Field array '" << this->glyphFunction3D->GetRotationFieldArrayName() << "' not found!"<< endl;
          this->SetRotationPtr = &Transformations::TransformScaleOff;
        }
        break;
      case ROTATE_OFF: // do nothing
        this->SetRotationPtr = &Transformations::TransformScaleOff;
        break;

      default:
        cout << "Rotation mode incorrect!" << endl;
    }
    // Setup displacement
    switch( this->DisplacementMode ){
      case DISPLACEMENT_BY_RANDOM: // generate random numbers
        this->SetDisplacementPtr = &Transformations::DisplaceGlyphByRandom;
        break;
      case DISPLACEMENT_BY_VECTOR: // use input vectors
        this->SetDisplacementPtr = &Transformations::DisplaceGlyphByVector;
        break;
      case DISPLACEMENT_BY_NORMAL: // use input normals
        this->SetDisplacementPtr = &Transformations::DisplaceGlyphByVector;
        break;
      case DISPLACEMENT_BY_FIELDARRAY: //
        this->DisplaceFieldArray = this->input->GetPointData()->GetArray(this->glyphFunction3D->GetDisplacementFieldArrayName().c_str());
        if (this->DisplaceFieldArray != nullptr) {
          this->SetDisplacementPtr = &Transformations::DisplaceGlyphByFieldArray;
        }
        else{
          cout << "Field array '" << this->glyphFunction3D->GetDisplacementFieldArrayName() << "' not found!" << endl;
          this->SetDisplacementPtr = &Transformations::TransformScaleOff;
        }
        break;
      case DISPLACEMENT_OFF: // do nothing
        this->SetDisplacementPtr = &Transformations::TransformScaleOff;
        break;
      default:
        cout << "Displacement mode incorrect!" << endl;
    }

    if (this->DisplacementMode != DISPLACEMENT_OFF) {
      if( this->input->GetNumberOfPoints() < 2 ){
        cout << "Not enough points for glyphing!" << endl;
      }
      else {
        input->GetPoint(0, c1);
        input->GetPoint(1, c2);
        v[0] = c1[0] - c2[0];
        v[1] = c1[1] - c2[1];
        v[2] = c1[2] - c2[2];
        p2pLength = vtkMath::Norm(v)/2.0;

        this->rng->SetSeed(8775070);
      }
    }

    this->hasScalars = this->scalars != nullptr ? true : false;
    this->hasVectors = this->vectors != nullptr ? true : false;
    this->hasNormals = this->normals != nullptr ? true : false;
    this->hasTensors = this->tensors != nullptr ? true : false;

    this->hasColorFieldArray = this->ColorFieldArray != nullptr ? true : false;
    this->hasScaleFieldArray = this->ScaleFieldArray != nullptr ? true : false;
    this->hasRotateFieldArray = this->RotateFieldArray != nullptr ? true : false;
    this->hasDisplaceFieldArray = this->DisplaceFieldArray != nullptr ? true : false;
  };
  ~Transformations(){
    this->transform->Delete();
    this->matrix->Delete();
    this->rng->Delete();
  };

  // Coloring
  double ColorGlyphByScalar(int pid){
    color = scalars->GetTuple1(pid);
    return color;
  };
  double ColorGlyphByVectorMagnitude(int pid){
    vectors->GetTuple(pid, v);
    color = vtkMath::Norm(v);
    return color;
  };
  double ColorGlyphByNormalMagnitude(int pid){
    normals->GetTuple(pid, v);
    color = vtkMath::Norm(v);
    return color;
  };
  double ColorGlyphByFieldArray(int pid){
    if( this->ColorFieldArray->GetNumberOfComponents() > 1 ){
      cout << "Field array used for coloring can only has more than 1 component. Will use magnitude."<< endl;
      this->ColorFieldArray->GetTuple(pid, v);
      color = vtkMath::Norm(v);
    }
    else {
      color = this->ColorFieldArray->GetTuple1(pid);;
    }
    return color;
  };

  // Scaling
  void ScaleGlyphByScalar(int pid){
    m = scalars->GetTuple1(pid);
    scale = isNull(m) ? this->eps : m * scaleFactor;
    transform->Scale(scale, scale, scale);
  };
  void ScaleGlyphByVectorMagnitude(int pid){
    vectors->GetTuple(pid, v);
    m = vtkMath::Norm(v);
    scale = isNull(m) ? this->eps : m * scaleFactor;
    transform->Scale(scale, scale, scale);
  };
  void ScaleGlyphByNormalMagnitude(int pid){
    normals->GetTuple(pid, v);
    m = vtkMath::Norm(v);
    scale = isNull(m) ? this->eps : m * scaleFactor;
    transform->Scale(scale, scale, scale);
  };
  void ScaleGlyphByVectorComponents(int pid){
    vectors->GetTuple(pid, v);
    v[0] *= scaleFactor;
    v[1] *= scaleFactor;
    v[2] *= scaleFactor;

    if( !isNull(v[0]) || !isNull(v[1]) || !isNull(v[2]) ){
      transform->Scale(v[0], v[1], v[2]);
    }
  };
  void ScaleGlyphByNormalComponents(int pid){
    normals->GetTuple(pid, v);
    v[0] *= scaleFactor;
    v[1] *= scaleFactor;
    v[2] *= scaleFactor;
    if( !isNull(v[0]) || !isNull(v[1]) || !isNull(v[2]) ){
      transform->Scale(v[0], v[1], v[2]);
    }
  };
  void ScaleGlyphByFieldArray(int pid){
    if( this->ScaleFieldArray->GetNumberOfComponents() > 1 ){
      this->ScaleFieldArray->GetTuple(pid, v);
      m = vtkMath::Norm(v);
      scale = isNull(m) ? this->eps : m * scaleFactor;
      transform->Scale(scale, scale, scale);
    }
    else {
      m = this->ScaleFieldArray->GetTuple1(pid);
      scale = isNull(m) ? this->eps : m * scaleFactor;
      transform->Scale(scale, scale, scale);
    }
  };

  // Rotation
  void RotateGlyphByVector(int pid){
    vectors->GetTuple(pid, v);
    if( !isNull(v[0]) || !isNull(v[1]) || !isNull(v[2]) ){
      transform->RotateWXYZ(180, v[0], v[1], v[2]);
    }
  };
  void RotateGlyphByNormal(int pid){
    normals->GetTuple(pid, v);
    if( !isNull(v[0]) || !isNull(v[1]) || !isNull(v[2]) ){
      transform->RotateWXYZ(180, v[0], v[1], v[2]);
    }
  };
  void RotateGlyphByTensor(int pid){
    tensors->GetTuple(pid, t);
    // represent tensor by matrix
    matrix->Element[0][0] = t[0];
    matrix->Element[0][1] = t[1];
    matrix->Element[0][2] = t[2];
    matrix->Element[1][0] = t[3];
    matrix->Element[1][1] = t[4];
    matrix->Element[1][2] = t[5];
    matrix->Element[2][0] = t[6];
    matrix->Element[2][1] = t[7];
    matrix->Element[2][2] = t[8];

    transform->Concatenate(matrix);
  };
  void RotateGlyphByFieldArray(int pid){
    if( this->RotateFieldArray->GetNumberOfComponents() >= 3 ){
      this->RotateFieldArray->GetTuple(pid, v);
      if( (!isNull(v[0]) || !isNull(v[1]) || !isNull(v[2])) ){
        transform->RotateWXYZ(180, v[0], v[1], v[2]);
      }
    }
    else {
      cout << "Cannot rotate glyphs using scalars! Verify RotateFieldArray" << endl;
    }
  };

  // Displacements
  void DisplaceGlyphByRandom(int pid){
    double x,y,z;
    x = this->rng->GetRangeValue(-this->p2pLength, this->p2pLength);
    this->rng->Next();
    y = this->rng->GetRangeValue(-this->p2pLength, this->p2pLength);
    this->rng->Next();
    z = this->rng->GetRangeValue(-this->p2pLength, this->p2pLength);
    this->rng->Next();
    transform->Translate(x, y, z);
  };
  void DisplaceGlyphByVector(int pid){
    this->vectors->GetTuple(pid, v);
    transform->Translate(v[0], v[1], v[2]);
  };
  void DisplaceGlyphByNormal(int pid){
    this->normals->GetTuple(pid, v);
    transform->Translate(v[0], v[1], v[2]);
  };
  void DisplaceGlyphByFieldArray(int pid){
    if( this->DisplaceFieldArray->GetNumberOfComponents() >= 3 ){
      this->DisplaceFieldArray->GetTuple(pid, v);
      if( !isNull(v[0]) || !isNull(v[1]) || !isNull(v[2]) ){
        transform->Translate(v[0], v[1], v[2]);
      }
    }
    else {
      cout << "Cannot displace glyphs using scalars! Verify DisplaceFieldArray" << endl;
    }
  };

  // Translations
  void SetTranslation(int pid){
    this->input->GetPoint(pid, v);
    transform->Translate(v[0], v[1], v[2]);
    this->SetDisplacement(pid);
  };

  void TransformScaleOff(int pid){};
  double TransformColorOff(int pid){return 0;};

  // -> Apply point transformations to glyphs
  void ApplyPointTransformations(vtkPoints* inPoints, vtkPoints* outPoints){
    transform->TransformPoints(inPoints, outPoints);
  };
  // -> Apply normal transformations to glyphs
  void ApplyNormalTransformations(vtkDataArray* inNormals, vtkDataArray* outNormals){
    if (transform->GetMatrix()->Determinant() < 0){
      transform->Scale(-1.0, -1.0, -1.0);
    }
    transform->TransformNormals(inNormals, outNormals);
  };

  
};

// vtkCxxSetObjectMacro(vtkGlyphFunction3D, vtkTransform);

//------------------------------------------------------------------------------
// Construct object with scaling on, scaling mode is by scalar value,
// scale factor = 1.0, the range is (0,1), orient geometry is on, and
// orientation is by vector. Clamping and indexing are turned off. No
// initial sources are defined.
vtkGlyphFunction3D::vtkGlyphFunction3D(){
  this->SetNumberOfInputPorts(2);
  this->SetNumberOfOutputPorts(1);

  this->SourceMethod = nullptr;
  this->SourceMethodArgDelete = nullptr;
  this->SourceMethodArg = nullptr;
  this->OutputPointsPrecision = vtkAlgorithm::DEFAULT_PRECISION;

  this->ScaleMode = SCALE_BY_VECTOR_COMPONENTS;
  this->ColorMode = COLOR_BY_SCALAR;
  this->RotationMode = ROTATE_OFF;
  this->DisplacementMode = DISPLACEMENT_OFF;
  this->Clamping = 0;
  this->ScaleFactor = 1;
  this->PointId = 0;
  this->Point[0] = 0;
  this->Point[1] = 0;
  this->Point[2] = 0;

  // by default process active point scalars
  this->SetInputArrayToProcess(
    0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, vtkDataSetAttributes::SCALARS);
  // by default process active point vectors
  this->SetInputArrayToProcess(
    1, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, vtkDataSetAttributes::VECTORS);
  // by default process active point normals
  this->SetInputArrayToProcess(
    2, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, vtkDataSetAttributes::NORMALS);
  // by default process active point tensors
  this->SetInputArrayToProcess(
    3, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, vtkDataSetAttributes::TENSORS);
}

//------------------------------------------------------------------------------
vtkGlyphFunction3D::~vtkGlyphFunction3D(){
  // delete the current arg if there is one and a delete meth
  if ((this->SourceMethodArg) && (this->SourceMethodArgDelete)){
    (*this->SourceMethodArgDelete)(this->SourceMethodArg);
  }
}

void vtkGlyphFunction3D::SetSourceConnection(vtkAlgorithmOutput* output){
  this->SetInputConnection(1, output);
}

void vtkGlyphFunction3D::SetSourceData(vtkPolyData* pd){
  this->SetInputData(1, pd);
}

// Get a pointer to a source object at a specified table location.
vtkPolyData* vtkGlyphFunction3D::GetSource(){
  if (this->GetNumberOfInputConnections(1) < 1){
    return nullptr;
  }
  return vtkPolyData::SafeDownCast(this->GetExecutive()->GetInputData(1, 0));
}

//------------------------------------------------------------------------------
void vtkGlyphFunction3D::PrintSelf(ostream& os, vtkIndent indent){
  this->Superclass::PrintSelf(os, indent);
  // TODO
  os << indent << "Output Points Precision: " << this->OutputPointsPrecision << "\n";
}

//------------------------------------------------------------------------------
vtkMTimeType vtkGlyphFunction3D::GetMTime(){
  return 1;
}

// Specify the function to use to generate the source data. Note
// that the function takes a single (void *) argument.
void vtkGlyphFunction3D::SetSourceMethod(void (*f)(void*), void* arg){
  if (f != this->SourceMethod || arg != this->SourceMethodArg){
    // delete the current arg if there is one and a delete meth
    if ((this->SourceMethodArg) && (this->SourceMethodArgDelete)){
      (*this->SourceMethodArgDelete)(this->SourceMethodArg);
    }
    this->SourceMethod = f;
    this->SourceMethodArg = arg;
    this->Modified();
  }
}

// Set the arg delete method. This is used to free user memory.
void vtkGlyphFunction3D::SetSourceMethodArgDelete(void (*f)(void*)){
  if (f != this->SourceMethodArgDelete){
    this->SourceMethodArgDelete = f;
    this->Modified();
  }
}

//------------------------------------------------------------------------------
int vtkGlyphFunction3D::RequestData(vtkInformation* vtkNotUsed(request),
  vtkInformationVector** inputVector, vtkInformationVector* outputVector){

  // get the info objects
  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation* outInfo = outputVector->GetInformationObject(0);

  // get the input and output
  vtkDataSet* input = vtkDataSet::SafeDownCast( inInfo->Get(vtkDataObject::DATA_OBJECT()) );
  vtkPolyData* output = vtkPolyData::SafeDownCast( outInfo->Get(vtkDataObject::DATA_OBJECT()) );

  assert(input && output);
  if (input == nullptr || output == nullptr){
    // nothing to do.
    return true;
  }

  // this is used to respect blanking specified on uniform grids.
  vtkUniformGrid* inputUG = vtkUniformGrid::SafeDownCast( input );

  // Input variables
  vtkPointData* inPD = input->GetPointData();
  int nbPts = input->GetNumberOfPoints();
  vtkDataArray* inVectors = this->GetInputArrayToProcess(1, input);
  vtkDataArray* inTCoords = inPD->GetTCoords();
  vtkDataArray* inTensors = this->GetInputArrayToProcess(3, input);
  vtkDataArray* temp = nullptr;
  unsigned char* inGhostLevels = nullptr;

  // Source variables
  vtkInformation* sourcePolyDataInfo;
  vtkPolyData* sourcePolyData;
  int nbSourcePts, nbSourceCells, nbCellPts;
  double color;
  vtkPoints* sourcePts = nullptr;
  vtkDataArray* sourceScalars = nullptr,
    *sourceNormals = nullptr, 
    *sourceTCoords = nullptr;
  vtkNew<vtkIdList> pointIdList;
  vtkIdList* pts;

  // Output variables
  vtkPointData* outPD = output->GetPointData();
  vtkCellData* outCD = output->GetCellData();
  vtkPoints* newPts;
  bool haveScalars = false, 
    haveVectors = false, 
    haveNormals = false, 
    haveTCoords = false, 
    haveTensors = false;
  vtkDataArray *newScalars = nullptr,
    *newVectors = nullptr,
    *newNormals = nullptr,
    *newTCoords = nullptr,
    *newTensors = nullptr;
  double t[9], tmp_v[3];
  vtkNew<vtkIdList> srcPointIdList;
  vtkNew<vtkIdList> dstPointIdList;
  vtkNew<vtkIdList> srcCellIdList;
  vtkNew<vtkIdList> dstCellIdList;

  vtkDebugMacro( << "Generating glyphs");

  pts = vtkIdList::New();
  pts->Allocate(VTK_CELL_SIZE);

  // First source function done apart
  // to get info on sourcePolyData
  if( this->SourceMethod != nullptr ){
    (*this->SourceMethod)(this->SourceMethodArg);
    if (this->GetNumberOfInputConnections(1) == 0){
      sourcePolyData = nullptr;
    }
    else{
      // Update the source connection in case the GlyphMethod changed
      // its parameters.
      this->GetInputAlgorithm(1, 0)->Update();
      // The GlyphMethod may also have changed the source.
      sourcePolyDataInfo = inputVector[1]->GetInformationObject(0);
      sourcePolyData = vtkPolyData::SafeDownCast(sourcePolyDataInfo->Get(vtkDataObject::DATA_OBJECT()));
      nbSourceCells = sourcePolyData->GetNumberOfCells();
      nbSourcePts = sourcePolyData->GetNumberOfPoints();
      sourceScalars = sourcePolyData->GetPointData()->GetScalars();
      sourceNormals = sourcePolyData->GetPointData()->GetNormals();
      sourceTCoords = sourcePolyData->GetPointData()->GetTCoords();
    }
  }

  if( nbPts < 1 ){
    vtkDebugMacro( << "No points to glyph!" );
    return true;
  }
  if( nbSourcePts < 1 ){
    vtkDebugMacro( << "No geometry on source glyph!" );
    return true;
  }
  if( nbSourceCells < 1 ){
    vtkDebugMacro( << "No topology on source glyph!" );
    return true;
  }

  newPts = vtkPoints::New();
  // Set the desired precision for the points in the output.
  if (this->OutputPointsPrecision == vtkAlgorithm::DEFAULT_PRECISION){
    newPts->SetDataType(VTK_FLOAT);
  }
  else if (this->OutputPointsPrecision == vtkAlgorithm::SINGLE_PRECISION){
    newPts->SetDataType(VTK_FLOAT);
  }
  else if (this->OutputPointsPrecision == vtkAlgorithm::DOUBLE_PRECISION){
    newPts->SetDataType(VTK_DOUBLE);
  }

  newPts->Allocate(nbPts * nbSourcePts);

  // Setting up for calls to PolyData::InsertNextCell()
  output->AllocateEstimate(nbPts * nbSourceCells, 3);

  // Setup output data
  Transformations transform(this, input);

  // Setup color data
  if( ColorMode != COLOR_OFF && 
      (transform.hasScalars ||
      transform.hasColorFieldArray) ){
    newScalars = vtkFloatArray::New();
    newScalars->SetName( "Scalars" );
    newScalars->SetNumberOfComponents(1);
    newScalars->Allocate(nbPts * nbSourcePts);
    haveScalars = true;
  }
  if( transform.hasVectors ){
    newVectors = vtkFloatArray::New();
    newVectors->SetNumberOfComponents(3);
    newVectors->Allocate( 3 * nbPts * nbSourcePts);
    newVectors->SetName("Vectors");
    haveVectors = true;
  }
  if( transform.hasNormals || sourceNormals != nullptr ){
    newNormals = vtkFloatArray::New();
    newNormals->SetNumberOfComponents(3);
    newNormals->Allocate(3 * nbPts * nbSourcePts);
    newNormals->SetName("Normals");
    haveNormals = true;
  }
  if( inTCoords != nullptr || sourceTCoords != nullptr ){
    newTCoords = vtkFloatArray::New();
    newTCoords->SetNumberOfComponents(3);
    newTCoords->Allocate(3 * nbPts * nbSourcePts);
    newTCoords->SetName("TCoords");
    haveTCoords = true;
  }
  if( inTensors != nullptr ){
    newTensors = vtkFloatArray::New();
    newTensors->SetNumberOfComponents(9);
    newTensors->Allocate(9 * nbPts * nbSourcePts);
    newTensors->SetName("Tensors");
    haveTensors = true;
  }

  // Ghost level suport
  if( inPD ){
    temp = inPD->GetArray( vtkDataSetAttributes::GhostArrayName() );
  }
  if( (!temp) || (temp->GetDataType() != VTK_UNSIGNED_CHAR) || (temp->GetNumberOfComponents() != 1) ){
    vtkDebugMacro( << "No appropriate ghost levels field available." );
  }
  else{
    inGhostLevels = static_cast<vtkUnsignedCharArray*>(temp)->GetPointer(0);
  }

  // Allocate storage for output PolyData
  outPD->CopyVectorsOff();
  outPD->CopyNormalsOff();
  outPD->CopyTCoordsOff();
  outPD->CopyAllocate(inPD, nbPts * nbSourcePts);

  srcPointIdList->SetNumberOfIds( nbSourcePts );
  dstPointIdList->SetNumberOfIds( nbSourcePts );
  srcCellIdList->SetNumberOfIds( nbSourceCells );
  dstCellIdList->SetNumberOfIds( nbSourceCells );

  int ptIncr = 0, cellIncr = 0, cellId = 0, i = 0;

  // Iterate over points of geometry
  for( int pid=0; pid<nbPts; pid++ ){
    transform.SetIdentity();
    this->PointId = pid;
    input->GetPoint(pid, this->Point);

    (*this->SourceMethod)(this->SourceMethodArg);
    if (this->GetNumberOfInputConnections(1) == 0){
      sourcePolyData = nullptr;
    }
    else{
      // Update the source connection in case the GlyphMethod changed
      // its parameters.
      this->GetInputAlgorithm(1, 0)->Update();
      // The GlyphMethod may also have changed the source.
      sourcePolyDataInfo = inputVector[1]->GetInformationObject(0);
      sourcePolyData = vtkPolyData::SafeDownCast(sourcePolyDataInfo->Get(vtkDataObject::DATA_OBJECT()));
    }
    if( sourcePolyData ){
      // Use source field data
      nbSourceCells = sourcePolyData->GetNumberOfCells();
      nbSourcePts = sourcePolyData->GetNumberOfPoints();
      sourceScalars = sourcePolyData->GetPointData()->GetScalars(); // scalar colors
      sourceNormals = sourcePolyData->GetPointData()->GetNormals(); // face normals
      sourceTCoords = sourcePolyData->GetPointData()->GetTCoords(); // texture coords
      sourcePts = sourcePolyData->GetPoints();
    }
    
    // Check ghost points.
    // If we are processing a piece, we do not want to duplicate glyphs on the borders.
    if( inGhostLevels &&
        inGhostLevels[pid] &
        (vtkDataSetAttributes::DUPLICATEPOINT | vtkDataSetAttributes::HIDDENPOINT) ){
      continue;
    }

    if( inputUG && !inputUG->IsPointVisible(pid) ){
      // input is a vtkUniformGrid and the current point is blanked. Don't glyph
      // it.
      continue;
    }

    if( !this->IsPointVisible(input, pid) ){
      continue;
    }

    // Copy all topology (transformation independent)
    for( cellId=0; cellId<nbSourceCells; cellId++ ){
      sourcePolyData->GetCellPoints(cellId, pointIdList);
      nbCellPts = pointIdList->GetNumberOfIds();
      for( pts->Reset(), i = 0; i < nbCellPts; i++ ){
        pts->InsertId(i, pointIdList->GetId(i) + ptIncr);
      }
      output->InsertNextCell(sourcePolyData->GetCellType(cellId), pts);
    }

    

    if( sourceScalars && ColorMode == COLOR_OFF ){
      for( i=0; i<nbSourcePts; i++ ){
        newScalars->InsertNextTuple1(sourceScalars->GetTuple1( i ));
      }
    }
    // Use input field data
    else if( haveScalars && ColorMode != COLOR_OFF ){
      color = transform.SetColor(pid);
      for( i=0; i<nbSourcePts; i++ ){
        newScalars->InsertTuple1(ptIncr + i, color);
      }
    }
    if( sourceTCoords ){
      for( i=0; i<nbSourcePts; i++ ){
        sourceTCoords->GetTuple(i, tmp_v);
        newTCoords->InsertTuple(ptIncr + i, tmp_v);
      }
    }
    if( haveVectors ){
      for( i=0; i<nbSourcePts; i++ ){
        inVectors->GetTuple(i, tmp_v);
        newVectors->InsertTuple(ptIncr + i, tmp_v);
      }
    }
    if( haveTensors ){
      for( i=0; i<nbSourcePts; i++ ){
        inTensors->GetTuple(i, t);
        newTensors->InsertTuple(ptIncr + i, t);
      }
    }

    transform.SetTranslation(pid);
    transform.SetScale(pid);
    transform.SetRotation(pid);
    transform.ApplyPointTransformations(sourcePts, newPts);

    if( sourceNormals != nullptr ){
      transform.ApplyNormalTransformations(sourceNormals, newNormals);
    }   

    // Copy point data from source (if possible)
    for( i = 0; i < nbSourcePts; ++i ){
      srcPointIdList->SetId(i, pid);
      dstPointIdList->SetId(i, ptIncr + i);
    }
    outPD->CopyData(inPD, srcPointIdList, dstPointIdList);

    for( i = 0; i < nbSourceCells; ++i ){
      srcCellIdList->SetId(i, pid);
      dstCellIdList->SetId(i, cellIncr + i);
    }
    outCD->CopyData(inPD, srcCellIdList, dstCellIdList);

    ptIncr += nbSourcePts;
    cellIncr += nbSourceCells;
  }

  // Update ourselves and release memory
  output->SetPoints( newPts );
  newPts->Delete();

  int idx;

  if( haveScalars ){
    idx = outPD->AddArray( newScalars );
    outPD->SetActiveAttribute(idx, vtkDataSetAttributes::SCALARS);
    newScalars->Delete();
  }
  if( haveVectors ){
    idx = outPD->AddArray( newVectors );
    outPD->SetActiveAttribute(idx, vtkDataSetAttributes::VECTORS);
    newVectors->Delete();
  }
  if( haveNormals ){
    idx = outPD->AddArray( newNormals );
    outPD->SetActiveAttribute(idx, vtkDataSetAttributes::NORMALS);
    newNormals->Delete();
  }
  if( haveTCoords ){
    idx = outPD->AddArray( newTCoords );
    outPD->SetActiveAttribute(idx, vtkDataSetAttributes::TCOORDS);
    newTCoords->Delete();
  }
  if( haveTensors ){
    idx = outPD->AddArray( newTensors );
    outPD->SetActiveAttribute(idx, vtkDataSetAttributes::TENSORS);
    newTensors->Delete();
  }

  output->Squeeze();
  pts->Delete();

  return 1;
}



int vtkGlyphFunction3D::RequestUpdateExtent(vtkInformation* vtkNotUsed(request),
  vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
  // get the info objects
  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation* outInfo = outputVector->GetInformationObject(0);

  inInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER(),
    outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER()));
  inInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES(),
    outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES()));
  inInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS(),
    outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS()));
  inInfo->Set(vtkStreamingDemandDrivenPipeline::EXACT_EXTENT(), 1);

  return 1;
}

//------------------------------------------------------------------------------
int vtkGlyphFunction3D::FillInputPortInformation(int port, vtkInformation* info)
{
  if (port == 0){
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
    return 1;
  }
  return 0;
}
